<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_initial extends CI_Migration {

        public function up()
        {
                // TABLE USERS
                $this->table_user();

                // TABLE USER PROFILE
                $this->table_user_profile();

                // TABLE GROUP TYPE
                $this->table_group_type();

                //TABLE GROUP
                $this->table_group();

                // TABLE MESSAGE TYPE
                $this->table_message_type();

                // TABLE MESSAGES 
                $this->table_messages();

                // TABLE KEYS FOR API
                $this->table_keys();

                // TABLE LOG ACCESS API
                $this->table_logs();
        }

        public function down()
        {
                $this->dbforge->drop_table('users');
                $this->dbforge->drop_table('user_profile');
                $this->dbforge->drop_table('group');
                $this->dbforge->drop_table('group_type');
        }

        private function table_user(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'username' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('users');
                
                $data = array(
                        'id' => '436be61d-78cc-48df-9181-2e0f1ef85da5',
                        'username' => 'admin',
                        'password' => '81dc9bdb52d04dc20036dbd8313ed055'
                );
                $this->db->insert('users',$data);
        }
        
        private function table_user_profile(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'full_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'short_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'phone_number' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                        'users_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('user_profile');

                $data = array(
                        'id' => 'b3e876d7-a929-4bc7-ac93-1b14cdd45a69',
                        'full_name' => 'Administrator',
                        'short_name' => 'Admin',
                        'phone_number' => '-',
                        'email' => 'admin@mail.com',
                        'users_id' => '436be61d-78cc-48df-9181-2e0f1ef85da5'
                );
                $this->db->insert('user_profile',$data);
        }

        private function table_group(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'group_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'group_description' => array(
                                'type' => 'TINYTEXT',
                        ),
                        'group_type_id' => array(
                                'type' => 'INT',
                                'constraint' => 1,
                        ),
                        'users_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('group');
        }

        private function table_group_type(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 2
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '45',
                        ),
                        'description' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('group_type');

                $data = array(
                        array(
                                'id' => 1,
                                'name' => 'Public',
                                'description' => 'Public Group',
                        ),
                        array(
                                'id' => 2,
                                'name' => 'Private',
                                'description' => 'Private Group',
                        ),
                        array(
                                'id' => 3,
                                'name' => 'Specific',
                                'description' => 'Specific Group',
                        ),
                );
                $this->db->insert_batch('group_type', $data);
        }

        private function table_message_type(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 2
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '45',
                        ),
                        'description' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('message_type');

                $data = array(
                        array(
                                'id' => 1,
                                'name' => 'Personal',
                                'description' => 'Personal Message',
                        ),
                        array(
                                'id' => 2,
                                'name' => 'Group',
                                'description' => 'Group Message',
                        ),
                );
                $this->db->insert_batch('message_type', $data);
        }

        private function table_messages(){
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'message' => array(
                                'type' => 'TEXT',
                        ),
                        'message_status' => array(
                                'type' => 'INT',
                                'constraint' => 1
                        ),
                        'parent' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'message_type_id' => array(
                                'type' => 'INT',
                                'constraint' => 2
                        ),
                        'users_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'group_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                                'null' => TRUE
                        )
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('messages');
        }

        private function table_keys()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'autoincrement' => TRUE
                        ),
                        'user_id' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'key' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '40'
                        ),
                        'level' => array(
                                'type' => 'INT',
                                'constraint' => 2
                        ),
                        'ignore_limits' => array(
                                'type' => 'tinyint',
                                'constraint' => 1
                        ),
                        'is_private_key' => array(
                                'type' => 'tinyint',
                                'constraint' => '1'
                        ),
                        'ip_addresses' => array(
                                'type' => 'TEXT'
                        ),
                        'date_created' => array(
                                'type' => 'DATETIME'
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('keys');
                $data = array(
                        'id' => 1,
                        'user_id' => '436be61d-78cc-48df-9181-2e0f1ef85da5',
                        'key' => 'KARLOTA@12345.',
                        'level' => 0,
                        'ignore_limits' => 0,
                        'is_private_key' => 0,
                        'ip_addresses' => $this->input->ip_address(),
                        'date_created' => date('Y-m-d H:i:s')
                );
                $this->db->insert('keys',$data);
        }

        private function table_logs()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'autoincrement' => TRUE
                        ),
                        'uri' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255'
                        ),
                        'method' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '6'
                        ),
                        'param' => array(
                                'type' => 'TEXT'
                        ),
                        'api_key' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '40'
                        ),
                        'ip_addresses' => array(
                                'type' => 'TEXT'
                        ),
                        'time' => array(
                                'type' => 'INT',
                                'constraint' => 11
                        ),
                        'rtime' => array(
                                'type' => 'FLOAT'
                        ),
                        'authorized' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1'
                        ),
                        'response_code' => array(
                                'type' => 'SMALLINT',
                                'constraint' => '3',
                                'default' => '0'
                        )
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('logs');
        }
}
