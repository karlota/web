<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->library('migration');
    }
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function migrate()
	{

		if ($this->migration->current() === FALSE)
		{
				show_error($this->migration->error_string());
		}
	}
}
