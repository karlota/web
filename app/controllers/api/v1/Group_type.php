<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Group_type extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_group_type');
    }

    public function index_get(){
        $group_type = $this->m_group_type->get_all();
        $id = $this->get('id');
        if ( $id === null ){
            if ($group_type){
                $this->response( $group_type, 200 );
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'No group type were found'
                ], 404 );
            }
        }
        else
        {
            $group_type = $this->m_group_type->get($id);
            if (!empty($group_type))
            {
                $this->response( $group_type, 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such group found'
                ], 404 );
            }
        }
    }

    public function index_post(){
        $data = $this->post();
        if($this->m_group_type->insert($data)){
            $this->response($data,201);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = $this->put();
        if ($this->m_group_type->update($id,$data)){
            $data = array_merge(array('id'=>$id),$data);
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        if($this->m_group_type->get($id)){
            $this->m_group_type->delete($id);
            $this->response(array('status'=>'success'),200);
        }else{
            $this->response(array('status'=>'fail'),404);
        }
    }
}