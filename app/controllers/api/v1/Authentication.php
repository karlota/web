<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;
class Authentication extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_user');
        $this->load->model('m_user_profile');
    }

    public function login_post(){
        $username = $this->post('username');
        $password = $this->post('password');

        if (!empty($username) && !empty($password)){
            $user = $this->m_users->get_by(array('username'=>$username));
            if ($user){
                $this->response(
                    array(
                        'status'=>TRUE,
                        'message' =>'User login successful.'
                    ),
                    RestController::HTTP_OK
                );
            }else{
                $this->response("Wrong username or password",RestController::HTTP_BAD_REQUEST);
            }
        }else{
            $this->response("Provide username and password",RestController::HTTP_BAD_REQUEST);
        }
    }

    public function registration_post(){
        $users_id = $this->uuid->v4();
        $profile_id = $this->uuid->v4();
        $result = array();
        $data_post = $this->post();

        $data_user = array(
            'id' => $users_id,
            'username' => $this->post('username'),
            'password' => md5($this->post('password'))
        );
        unset($data_post['username']);
        unset($data_post['password']);
        $data_profile = $data_post;
        if (!$this->m_users->get_by(array('username'=>$this->post('username')))){
            if($this->m_users->insert($data_user)){
                //assign users_id to array profile
                $data_profile = array_merge(array('users_id'=>$users_id),$data_profile);
                $this->m_user_profile->insert($data_profile);
                //response data
                $res_arr_add = array(
                    'id' => $profile_id,
                    'username'=>$this->post('username'),
                    'password'=>$this->post('password')
                );
                $data_profile = array_merge($res_arr_add,$data_profile);
                $this->response($data_profile,RestController::HTTP_CREATED);
            }else{
                $this->response("Some problems occurred, please try again",RestController::HTTP_BAD_REQUEST);
            }
        }else{
            $this->response(array('status'=>false,'message'=>'username already taken'),409);
        }
    }
}