<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class User_profile extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_user_profile');
    }

    public function index_get(){
        $profile = $this->m_user_profile->get_all();
        $id = $this->get('id');
        if ( $id === null ){
            if ($profile){
                $this->response( $profile, 200 );
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'No profile were found'
                ], 404 );
            }
        }
        else
        {
            $profile = $this->m_user_profile->get($id);
            if (!empty($profile))
            {
                $this->response( $profile, 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such profil found'
                ], 404 );
            }
        }
    }

    public function index_post(){
        $id = $this->uuid->v4();
        $data = $this->post();
        //check if userid exist
        if (!$this->m_users->get($this->post('users_id'))){
            $data = array_merge(array('id'=>$id),$data);
            if($this->m_user_profile->insert($data)){
                $this->response($data,201);
            }else{
                $this->response(array('status'=>'fail'),502);
            }
        }else{
            $this->response(array('status'=>false,'message'=>'User id not found'),404);
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = $this->put();
        if ($this->m_user_profile->update($id,$data)){
            $data = array_merge(array('id'=>$id),$data);
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        if($this->m_user_profile->get($id)){
            $this->m_user_profile->delete($id);
            $this->response(array('status'=>'success'),200);
        }else{
            $this->response(array('status'=>'fail'),404);
        }
    }
}