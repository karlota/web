<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Messages extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_messages');
        $this->load->model('m_message_type');
        $this->load->model('m_users');
    }

    public function index_get(){
        $group = $this->m_messages->get_all();
        $id = $this->get('id');
        if ( $id === null ){
            if ($group){
                $this->response( $group, 200 );
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'No message were found'
                ], 404 );
            }
        }
        else
        {
            $group = $this->m_messages->get($id);
            if (!empty($group))
            {
                $this->response( $group, 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such message found'
                ], 404 );
            }
        }
    }

    public function index_post(){
        $id = $this->uuid->v4();
        $data = $this->post();

        //cek message type
        $message_type = $this->m_messages_type->get($this->post('message_type'));

        //cek user
        $user = $this->m_users->get($this->post('users_id'));

        if (!empty($user) && !empty($group_type)){
            $data = array_merge(array('id'=>$id),$data);
            if($this->m_messages->insert($data)){
                $this->response($data,201);
            }else{
                $this->response(array('status'=>'fail'),502);
            }
        }else{
            $this->response(array('status'=>false,'message'=>'User or message type not found'),404);
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = $this->put();
        if ($this->m_messages->update($id,$data)){
            $data = array_merge(array('id'=>$id),$data);
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        if($this->m_messages->get($id)){
            $this->m_messages->delete($id);
            $this->response(array('status'=>'success'),200);
        }else{
            $this->response(array('status'=>'fail'),404);
        }
    }
}