<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Users extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_users');
    }

    public function index_get(){
        $users = $this->m_users->get_all();
        $id = $this->get('id');
        if ( $id === null ){
            if ($users){
                $this->response( $users, 200 );
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'No users were found'
                ], 404 );
            }
        }
        else
        {
            $user = $this->m_users->get($id);
            if (!empty($user))
            {
                $this->response( $user, 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such user found'
                ], 404 );
            }
        }
    }

    public function index_post(){
        $id = $this->uuid->v4();
        $data = array(
            'id' => $id,
            'username' => $this->post('username'),
            'password' => md5($this->post('password'))
        );
        if($this->m_users->insert($data)){
            $this->response($data,201);
        }else{
            $this->response(array('status'=>'fail',502));
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = array(
            'id' => $this->put('id'),
            'username' => $this->put('username'),
            'password' => $password
        );
        if ($this->m_users->update($id,$data)){
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        if($this->m_users->get($id)){
            $this->m_users->delete($id);
            $this->response(array('status'=>'success'),200);
        }else{
            $this->response(array('status'=>'fail'),404);
        }
    }
}