<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Message_type extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('m_message_type');
    }

    public function index_get(){
        $message_type = $this->m_message_type->get_all();
        $id = $this->get('id');
        if ( $id === null ){
            if ($message_type){
                $this->response( $message_type, 200 );
            }else{
                $this->response( [
                    'status' => false,
                    'message' => 'No message type were found'
                ], 404 );
            }
        }
        else
        {
            $message_type = $this->m_message_type->get($id);
            if (!empty($message_type))
            {
                $this->response( $message_type, 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such message type found'
                ], 404 );
            }
        }
    }

    public function index_post(){
        $data = $this->post();
        if($this->m_message_type->insert($data)){
            $this->response($data,201);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_put(){
        $id = $this->put('id');
        $data = $this->put();
        if ($this->m_message_type->update($id,$data)){
            $data = array_merge(array('id'=>$id),$data);
            $this->response($data,200);
        }else{
            $this->response(array('status'=>'fail'),502);
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        if($this->m_message_type->get($id)){
            $this->m_message_type->delete($id);
            $this->response(array('status'=>'success'),200);
        }else{
            $this->response(array('status'=>'fail'),404);
        }
    }
}