<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_message_type extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('message_type', 'id');
    }
}