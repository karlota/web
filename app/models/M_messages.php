<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_messages extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('messages', 'id',TRUE);
    }
}