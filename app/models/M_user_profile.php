<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_user_profile extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('user_profile', 'id',TRUE);
    }
}