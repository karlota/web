<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Model extends CI_Model {

    private $table;
    private $pk;
    private $group_db;
    private $extra_field = array(
        'created_date' => array(
            'created_date' => array(
                'type' => 'DATETIME'
            )
        ),
        'updated_date' => array(
            'updated_date' => array(
                'type' => 'DATETIME'
            )
        ),
        'author' => array(
            'author' => array(
                'type' => 'VARCHAR',
                'constraint' => 255
            )
        )
    );
    private $has_extra_field = FALSE;

    public function __construct() {
        parent::__construct();
        $this->group_db = 'default';
    }

    public function set_table($table = '', $pk = '', $extra_field = FALSE, $group_db = 'default') {
        $this->table = $table;
        $this->pk = $pk;
        $this->group_db = $group_db;
        $this->db = $this->load->database($this->group_db, TRUE);
        $this->dbforge = $this->load->dbforge($this->group_db, TRUE);
        $this->audit_table();
        if ($extra_field) {
            foreach ($this->extra_field as $key => $val) {
                if (!$this->db->field_exists($key, $this->table)) {
                    $this->dbforge->add_column($this->table, $val);
                }
            }
            $this->has_extra_field = TRUE;
        }
        return $this;
    }

    public function get_all($order = 'desc', $limit = FALSE) {
        if ($limit == TRUE) {
            $this->db->limit($limit);
        }
        $this->db->order_by($this->pk . ' ' . $order);
        return $this->_get()->result();
    }

    public function get_array() {
        return $this->_get()->result_array();
    }

    public function get_last_id() {
        $this->db->limit('1');
        $this->db->order_by($this->pk . ' desc');
        return $this->_get()->row();
    }

    public function get($id = '0') {
        $this->db->where($this->pk, $id);
        return $this->_get()->row();
    }

    public function get_by($param) {
        if (is_array($param)) {
            $this->db->where($param);
            return $this->_get()->row();
        }
        return FALSE;
    }

    public function get_many_by($param, $order = 'asc') {
        if (is_array($param)) {
            $this->db->where($param);
            return $this->get_all($order);
        }
        return FALSE;
    }
    
    public function set_order_by($param) {
        $this->db->order_by($param);
    }
    
    public function set_like($title,$match,$options='both') {
        $this->db->like($title, $match,$options);
    }

    public function get_join_where_id($table, $kondisi, $id) {
        $this->db->join($table, $kondisi);
        $this->db->where(array($this->pk => $id));
        return $this->get_all();
    }

    public function get_join($table, $kondisi) {
        $this->db->join($table, $kondisi);
        return $this->get_all();
    }

    public function insert($data = array()) {
        if ($this->has_extra_field) {
            $extra_data = array(
                'created_date' => date('Y-m-d H:i:s'),
                'updated_date' => date('Y-m-d H:i:s'),
                'author' => $this->session->userdata('username')
            );
            $data = array_merge($data, $extra_data);
        }
        if ($this->db->insert($this->table, $data)) {
            $this->audit('INSERT', $this->db->set($data)->get_compiled_insert($this->table));
            return true;
        }
        return false;
    }

    public function insert_batch($data = array()) {
        // if ($this->has_extra_field) {
        //     $extra_data = array(
        //         'created_date' => date('Y-m-d H:i:s'),
        //         'updated_date' => date('Y-m-d H:i:s'),
        //         'author' => $this->session->userdata('username')
        //     );
        //     $data = array_merge($data, $extra_data);
        // }
        if ($this->db->insert_batch($this->table, $data)) {
            return true;
        }
        return false;
    }

    public function insert_ignore($data = array()) {
        $insert_query = $this->db->insert_string($this->table, $data);
        $insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
        if ($this->db->query($insert_query)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function replace($data = array()){
        if ($this->db->replace($data)){
            return $this->db->replace_id();
        }
        return false;
    }

    public function delete($id = 0) {
        if ($this->db->delete($this->table, array($this->pk => $id))) {
            $this->audit('DELETE', $this->db->where($this->pk, $id)->get_compiled_delete($this->table));
            return true;
        }
        return false;
    }

    public function delete_by($by) {
        if ($this->db->delete($this->table, $by)) {
            $this->audit('DELETE BY', $this->db->where($by)->get_compiled_delete($this->table));
            return true;
        }
        return false;
    }

    public function simple_delete($id = 0) {
        if ($this->db->simple_query('delete FROM ' . $this->table . ' where ' . $this->pk . '=' . $id)) {
            return true;
        }
        return false;
    }

    public function update($id = 0, $data = array(), $audit = TRUE) {
        if ($this->has_extra_field) {
            $extra_data = array(
                'updated_date' => date('Y-m-d H:i:s'),
                'author' => $this->session->userdata('username')
            );
            $data = array_merge($data, $extra_data);
        }
        $this->db->where(array($this->pk => $id));
        if ($this->db->update($this->table, $data)) {
            $this->audit('UPDATE', $this->db->set($data)->get_compiled_update($this->table));
            return true;
        }
        return false;
    }

    public function update_batch($data = array()) {
        if ($this->db->update_batch($this->table, $data, $this->pk)) {
            return true;
        }
        return false;
    }

    public function update_by($where = array(), $data = array()) {
        if ($this->has_extra_field) {
            $extra_data = array(
                'updated_date' => date('Y-m-d H:i:s'),
                'author' => $this->session->userdata('username')
            );
            $data = array_merge($data, $extra_data);
        }
        $this->db->where($where);
        if ($this->db->update($this->table, $data)) {
            $this->audit('UPDATE BY', $this->db->set($data)->where($where)->get_compiled_update($this->table));
            return true;
        }
        return false;
    }

    protected function _get() {
        return $this->db->get($this->table);
    }

    protected function audit($action, $data) {
        if ($this->session->userdata('username') === null){
            $auditor = 'unknown';
        }else{
            $auditor = $this->session->userdata('username');
        }
        $data = array(
            'audit_url' => current_url(),
            'auditor' => $auditor,
            'ip_address' => $this->input->ip_address(),
            'audit_date' => date('Y-m-d H:i:s'),
            'audit_table' => $this->table,
            'audit_action' => $action,
            'audit_data' => $data
        );
        $this->db->set('kode', 'UUID()', FALSE);
        $this->db->insert('smart_audit', $data);
    }

    protected function audit_table() {
        if (!$this->db->table_exists('smart_audit')) {
            $fields = array(
                'kode' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                ),
                'audit_url' => array(
                    'type' => 'TINYTEXT'
                ),
                'auditor' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                ),
                'ip_address' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'audit_date' => array(
                    'type' => 'DATETIME'
                ),
                'audit_table' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                ),
                'audit_action' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'audit_data' => array(
                    'type' => 'TEXT'
                )
            );
            $this->dbforge->add_field($fields);
            $this->dbforge->add_key('kode', TRUE);
            $this->dbforge->create_table('smart_audit',TRUE);
        }
    }

}

?>